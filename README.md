# microservice-project-otus
URL project : http://kvosems.ru

Развертывание приложения Online Boutique от Google. 

## Terraform Yandex Cloud Managed Service for Kubernetes
Для автоматизации развертывания Managed Service for Kubernetes в Yandex Cloud используется Terraform, для его использования необходимо выполнить след. шаги:
1. В косноле Yandex Cloud необходимо создать платежный аккаунт или убедиться в его наличии. Используя инструкцию по ссылке: [подготовка](https://yandex.cloud/ru/docs/managed-kubernetes/operations/kubernetes-cluster/kubernetes-cluster-create)
2. Далее необходимо подготовить Terrafrom к работе, выполнить это необходимо по следующей инструкции: [установка terraform](https://yandex.cloud/ru/docs/tutorials/infrastructure-management/terraform-quickstart#cli_1). Инструкция показывает об установке terraform, создании данных для аутентификации, настройки провайдера, конфигурации terraform.
3. Конфигурация terraform:

   ```terraform/cluster.tf``` - описание конфигурации Managed Service for Kubernetes.
   
   ```terraform/nodes_group.tf``` - описание конфигурации групп узлов worker node кластера k8s.
5. Проверка созданной конфигурации:
   ```
   terraform plan
   ```
6. Применение созданной конфигурации:
   ```
   terraform apply
   ```
## Настройка Gitlab CI для Terraform
Настройка Gitlab CI для Terraform реализована в рамках отдельного проекта: https://gitlab.com/shakhovichkirill/terraform_yandex_cloud
Для использования Gitlab CI в рамках развертывания Managed Service for Kubernetes в Yandex Cloud с помощью Terraform необходимо:
1. Реализовать загрузку состояний в S3 хранилище. Для этого выполнить действия согласно документации [инструкция](https://yandex.cloud/ru/docs/tutorials/infrastructure-management/terraform-state-storage)
2. Реализовать блокировку состояний Terraform c помощью YDB. Алгоритм действий описан в документации [инструкция](https://yandex.cloud/ru/docs/tutorials/infrastructure-management/terraform-state-lock)
3. Также для использования pipeline необходима передача переменных для авторизации:

   AWS_ACCESS_KEY_ID данная переменая равняется id access-key

   AWS_SECRET_ACCESS_KEY данная переменная равняется secret access-key

   команда для создания access-key сервисного аккаунта
   ```
   yc iam access-key create --service-account-name teradmin
   ```

4. Также для provider yandex terraform потребуются данные для авторизации на основе токена или ключа. Реализация  на основе файла ключа:
   Соаздадим переменную YC_KEY в vars проекта terraform, значение переменной можно получить из след. команды      создания ключа для сервисного аккаунта:
   ```
   yc iam key create --service-account-name teradmin --output sa-key.json
   ```


## Работа с кластером k8s
1. Установка yc cli [инструкция](https://yandex.cloud/ru/docs/cli/operations/install-cli)
2. Установка kubectl [инструкция](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
3. Далее сконфигурируем конфиг файл для kubectl
   ```
   yc k8s cluster get-credentials k8s-project --folder-id <folder-id> --external
   ```
4. Проверка работы kubectl
   ```
   kubectl get nodes
   ```
5. Taint and labels для групп узлов infra
   ```
   kubectl taint nodes prod-awan-cl10l1bci20paorqmrpg node-role=k8s-project-infra:NoSchedule
   kubectl label nodes prod-awan-cl10l1bci20paorqmrpg infra=true
   ```
   
## ArgoCD
Для развертывания самого приложения online boutique используется инструмент автоматизированного развертывания приложений ArgoCD. Установка ArgoCD и создание проекта прилодения описаны ниже. 
1. Добавляем репозиторий и устанавливаем ArgoCD в кластер. Предварительно создав values.yaml.
   ```
   helm repo add argo https://argoproj.github.io/argo-helm
   helm upgrade --install argocd argo/argo-cd -f values.yaml -n argocd --create-namespace
   ```
2. Создаем проект
   ```
   kubectl apply -f project.yaml
   ``` 
3. Создаем приложение
   ```
   kubectl apply -f app-microservices-demo.yaml
   ```
Далее развертывание микросервисов порисходит в ArgoCD.

![Скриншот ArgoCD](https://gitlab.com/shakhovichkirill/microservice-project-otus/-/blob/main/screenshots/ArgoCD.png?raw=true)

## Мониторинг
1. Устанавливаем в кластер Prometheus-operator через helm 
   ```
   helm install prometheus oci://registry-1.docker.io/bitnamicharts/kube-prometheus
   ```
2. После установки Grafana в кластер kubernetes добавляем источник данных prometheus и добавляем dashboards для мониторинга pods, nodes и т.д.

![Скриншот Мониторинг Pods](https://gitlab.com/shakhovichkirill/microservice-project-otus/-/blob/main/screenshots/kubernetes%20monitoring%20pods.png?raw=true)

![Скриншот Мониторинг Nodes](https://gitlab.com/shakhovichkirill/microservice-project-otus/-/blob/main/screenshots/kubernetes%20monitoring%20nodes.png?raw=true)


## Логирование
1. Создаем бакет в s3 object storage Yandex cloud. Создаем сервисный аккаунт для s3 и назначаем определенные роли, согласно инструкциям Yandex Cloud.
2. Добавляем репозиторий helm 
   ```
   helm repo add grafana https://grafana.github.io/helm-charts
   ```
3. В кластер устанавливаем loki
   ```
   helm upgrade --values loki-values.yml --install loki --namespace=loki grafana/loki --create-namespace
   ```
4. Устанавливаем promtail
   ```
   helm upgrade --values promtail-values.yml --install promtail --namespace=loki grafana/promtail
   ```
5. Устанавливаем Grafana
   ```
   helm upgrade --values grafa-value.yml --install grafana --namespace=loki grafana/grafana
   ```

![Скриншот Loki](https://gitlab.com/shakhovichkirill/microservice-project-otus/-/blob/main/screenshots/kubernetes%20monitoring%20loki.png?raw=true)
